OUTDIR ?= .out
LIBDIR ?= /usr/lib/firefox
ETCDIR ?= /etc/firefox
PACDIR ?= /etc/pacman.d/conf.d

# Targets: --------------------------------------------------------------------------------------------------------

all: pacman policies settings styles

#------------------------------------------------------------------------------------------------------------------

pacman: $(OUTDIR)/pacman/firefox.conf

$(OUTDIR)/pacman/%: pacman/% | $(OUTDIR)/pacman
	cp -a '$<' '$@'

#------------------------------------------------------------------------------------------------------------------

policies: $(OUTDIR)/policies/policies.json

$(OUTDIR)/policies/policies.json: policies/policies.json | $(OUTDIR)/policies
	sed -E -e '/^$$/d' -e '\!^[[:space:]]*//!d' '$<' | jq --indent 3 -S . >'$@'

#------------------------------------------------------------------------------------------------------------------

settings: $(OUTDIR)/settings/autoconfig.js $(OUTDIR)/settings/mozilla.cfg $(OUTDIR)/settings/user.js

$(OUTDIR)/settings/%: settings/% | $(OUTDIR)/settings
	sed -E                                              \
		-e '\!^(//:|$$)!d'                               \
		-e '/^I\s+(\S+)/d'                               \
		-e 's/^c\s+(\S+)/clearPref("\1");/'              \
		-e 's/^d\s+(\S+)\s+(.+)/defaultPref("\1", \2);/' \
		-e 's/^l\s+(\S+)\s+(.+)/lockPref("\1", \2);/'    \
		-e 's/^p\s+(\S+)\s+(.+)/pref("\1", \2);/'        \
		<'$<' >'$@'

$(OUTDIR)/settings/user.js: $(OUTDIR)/settings/mozilla.cfg
	sed -E -e '/^\/\//d' -e '/^clearPref\(/d' -e 's/^(lockPref|pref|defaultPref)\(/user_pref(/' <'$<' >'$@'

.INTERMEDIATE: settings/mozilla.cfg
settings/mozilla.cfg: settings/mozilla.tpl
	./cfgmk >'$@'

#------------------------------------------------------------------------------------------------------------------

styles: $(OUTDIR)/styles/userChrome.css $(OUTDIR)/styles/userContent.css

$(OUTDIR)/styles/%: styles/% | $(OUTDIR)/styles
	cp -a '$<' '$@'

#------------------------------------------------------------------------------------------------------------------

$(OUTDIR)/pacman $(OUTDIR)/policies $(OUTDIR)/settings $(OUTDIR)/styles:
	mkdir -p '$@'

#------------------------------------------------------------------------------------------------------------------

clean:
	rm -rf $(OUTDIR)

#------------------------------------------------------------------------------------------------------------------

install: install-pacman install-policies install-settings install-styles

#------------------------------------------------------------------------------------------------------------------

install-pacman: pacman | $(DESTDIR)/$(PACDIR)
	install -m 0644 -D $(OUTDIR)/pacman/firefox.conf $(DESTDIR)/$(PACDIR)/firefox.conf

#------------------------------------------------------------------------------------------------------------------

install-policies: policies | $(DESTDIR)/$(ETCDIR) $(DESTDIR)/$(LIBDIR)/distribution
	install -m 0644 -D $(OUTDIR)/policies/policies.json $(DESTDIR)/$(ETCDIR)/policies.json
	ln -s $(ETCDIR)/policies.json $(DESTDIR)/$(LIBDIR)/distribution/policies.json

#------------------------------------------------------------------------------------------------------------------

install-settings: settings | $(DESTDIR)/$(ETCDIR) $(DESTDIR)/$(LIBDIR)/defaults/pref
	install -m 0644 -D $(OUTDIR)/settings/autoconfig.js $(DESTDIR)/$(ETCDIR)/autoconfig.js
	install -m 0644 -D $(OUTDIR)/settings/mozilla.cfg   $(DESTDIR)/$(ETCDIR)/mozilla.cfg
	install -m 0644 -D $(OUTDIR)/settings/user.js       $(DESTDIR)/$(ETCDIR)/user.js
	ln -s $(ETCDIR)/autoconfig.js $(DESTDIR)/$(LIBDIR)/defaults/pref/autoconfig.js
	ln -s $(ETCDIR)/mozilla.cfg   $(DESTDIR)/$(LIBDIR)/mozilla.cfg

#------------------------------------------------------------------------------------------------------------------

install-styles: styles | $(DESTDIR)/$(ETCDIR)
	install -m 0644 -D $(OUTDIR)/styles/userChrome.css  $(DESTDIR)/$(ETCDIR)/userChrome.css
	install -m 0644 -D $(OUTDIR)/styles/userContent.css $(DESTDIR)/$(ETCDIR)/userContent.css

#------------------------------------------------------------------------------------------------------------------

$(DESTDIR)/$(ETCDIR) $(DESTDIR)/$(LIBDIR)/defaults/pref $(DESTDIR)/$(LIBDIR)/distribution $(DESTDIR)/$(PACDIR):
	mkdir -p '$@'

#------------------------------------------------------------------------------------------------------------------

.PHONY: all clean pacman policies settings styles
.PHONY: install install-pacman install-policies install-settings install-styles

.SUFFIXES:

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=115: ##############################################